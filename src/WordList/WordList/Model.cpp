typedef struct Node
{
	void* data;
	Node* next;
	Node* prev;
};

typedef struct List
{
	int count;
	Node* first;
	Node* last;
};
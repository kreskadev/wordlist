#include "Header.h"
#include <stdio.h>
#include <stdlib.h>



List* ListCreate()
{
	List* list = (List*)(malloc(sizeof(List)));
	list->first = NULL;
	list->last = NULL;
	list->count = 0;

	return list;
}

bool ListDestroy(List* list)
{
	if(!IsEmptyList(list))
	{
		return false;
	}

	free(list);
}

void ListRemoveNode(List* list, Node* node)
{
	ListRemoveNode(list, node->data);
}

void ListRemoveNode(List* list, void* data)
{
	Node* current = list->first;
	while (current->next != NULL)
	{
		if(current->data != data)
		{
			continue;
		}

		Node* prev = current->prev;
		Node* next = current->next;
		prev->next = next;
		next->prev = prev;

		free(current);
		list->count--;
		return;
	}
}

void ListAddNode(List* list, void* data)
{
	Node* node = CreateNewNode(data);

	if(IsEmptyList(list))
	{
		AddFirstElement(list, node);
	}
	else
	{
		AddSingleElement(list, node);
	}

	list->count++;
}

bool IsEmptyList(List* list)
{
	return (list->count == 0);
}

void AddSingleElement(List* list, Node* node)
{
	Node* penultimate = list->last;
	penultimate->next = node;
	node->prev = penultimate;
	list->last = node;
}

void AddFirstElement(List* list, Node* node)
{
	list->first = node;
	list->last = node;
}

Node* CreateNewNode(void* data)
{
	Node* newNode = (Node*)(malloc(sizeof(Node)));
	newNode->next = NULL;
	newNode->prev = NULL;
	newNode->data = data;
	return newNode;
}

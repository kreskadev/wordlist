#include <stdio.h>

void WelcomeMessage()
{
	printf("This is WorldList app.\nYou can add new words, sort them, and list it in comand prop.\n");
}

int Menu()
{
	printf("Menu:\n");
	printf("1. Print words\n");
	printf("2. Add word\n");
	printf("3. Remove word\n");
	printf("4. Sort words\n");
	printf("5. Save 2 file\n");
	printf("6. Load from file\n");

	printf("0. Exit\n");
	
	int result = 0;
	printf("Select number:");
	scanf_s("%d", result);
	return result;
}

void ErrorMsg(const char* message)
{
	printf(message);
}

#include "Model.cpp"

void WelcomeMessage();
int Menu();
List* ListCreate();
bool ListDestroy(List* list);
void ListRemoveNode(List* list, Node* node);
void ListRemoveNode(List* list, void* data);
void ListAddNode(List* list, void* data);
bool IsEmptyList(List* list);
void AddSingleElement(List* list, Node* node);
void AddFirstElement(List* list, Node* node);
Node* CreateNewNode(void* data);
